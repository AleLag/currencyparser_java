import org.apache.log4j.Logger;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class AppCloser extends WindowAdapter {

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());
        windowEvent.getWindow().dispose();
        logger.info("Application terminated");
    }
}
