import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//Klasa do przeskakiwania miedzy tabelami walut
public class ButtonListener implements ActionListener {
    private static JComboBox<String> njcb;
    private static JLabel njl;
    private static JLabel njField;


    //referencje ktore sa przekazywane
    ButtonListener(JComboBox<String> jcb, JLabel jl,JLabel jtf)
    {
        njcb = jcb;
        njl = jl;
        njField = jtf;
    }


    public void actionPerformed(ActionEvent event)
    {
        Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());


        String ref;
        //w zaleznosci od flagi, wykonaj akcje
        if(Parser.getTable())
        {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(GUI.getGUISymbolsB());
            njcb.setModel(model);
            njl.setText("Today's currency rate: \n" + Parser.getValuesB().get(0) + " PLN");
            njField.setText("Name (pl): "+Parser.getNamesB().get(0));
            ref = "B";
            Parser.setTable(false);
        }
        else
        {
            DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(GUI.getGUISymbolsA());
            njcb.setModel(model);
            njl.setText("Today's currency rate: \n" + Parser.getValues().get(0) + " PLN");
            njField.setText("Name (pl): "+Parser.getNames().get(0));
            ref = "A";
            Parser.setTable(true);
        }

        logger.info("Switched table to " + ref);
    }

}
