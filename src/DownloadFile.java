import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
// Klasa do pobierania XML i ekstrakcji danych. Póki co prototyp
class DownloadFile {
    private static String uri = "http://api.nbp.pl/api/exchangerates/rates/";
    private static ArrayList<Double> histValues =  new ArrayList<>();
    private static ArrayList<Date> histDates =  new ArrayList<>();
    private static String codeName ="";
    static void downloadXLM(String choice) throws Exception
    {

        //Data z dzisiaj
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        //Miesieczny zakres
        String now = currentDate.format(formatter);
        String ago = currentDate.minusDays(30).format(formatter);
        if(Parser.getTable())
        {
            uri = uri+ "a/" + choice + "/" + ago + "/" + now;
        }
        else
        {
            uri = uri+ "b/" + choice + "/" + ago + "/" + now;
        }

        //pobieranie xmla przez GET
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection)  url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept","application/xml");

        InputStream xml = connection.getInputStream();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(xml);
        //Ekstrakcja danych
       // Element root = document.getDocumentElement();
        //System.out.println(root.getNodeName());
        codeName = document.getElementsByTagName("Code").item(0).getTextContent();
        NodeList nList = document.getElementsByTagName("Rate");


        for (int i = 0; i < nList.getLength(); i++)
        {
            Node node = nList.item(i);


            if(node.getNodeType() == Node.ELEMENT_NODE)
            {
                Element element = (Element) node;

                double tempVal = Double.parseDouble(element.getElementsByTagName("Mid").item(0).getTextContent());
                Date tempDate = new SimpleDateFormat("yyyy-MM-dd").parse(element.getElementsByTagName("EffectiveDate").item(0).getTextContent());
                histValues.add(tempVal);
                histDates.add(tempDate);
            }
        }




    }

    static ArrayList<Double> getHistValues()
    {
        return DownloadFile.histValues;
    }

    static ArrayList<Date> getHistDates()
    {
        return DownloadFile.histDates;
    }

    static String getCode()
    {
        return DownloadFile.codeName;
    }

    static void clearURI()
    {
        DownloadFile.uri = "http://api.nbp.pl/api/exchangerates/rates/";
    }
}
