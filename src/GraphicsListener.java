import org.apache.log4j.Logger;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Listener dla przycisku od tworzenia grafu
public class GraphicsListener implements ActionListener

{


    private static JComboBox njcb;
    GraphicsListener(JComboBox ijcb)
    {
        njcb = ijcb;
    }

    public void actionPerformed(ActionEvent event)
    {
        Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());

        DownloadFile.clearURI();
        String listChoice =(String) njcb.getSelectedItem();



        assert listChoice != null;

        String choice = listChoice.replaceAll("^[\\s.\\d]+", "").toLowerCase();
        logger.info("Monthly chart of " + choice.toUpperCase() + " is being drawn and opened");
        try {
            DownloadFile.downloadXLM(choice);
        } catch (Exception e) {

            logger.error("Not on list", e);
            e.printStackTrace();
        }
        SwingUtilities.invokeLater(() -> {
            Plot newChart = new Plot("New currency chart");
            newChart.setSize(400,400);
            newChart.setLocationRelativeTo(null);
            newChart.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            newChart.addWindowListener(new MyCleaner(choice.toUpperCase()));
            newChart.setVisible(true);
        });
    }
}


