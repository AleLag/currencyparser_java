import org.apache.log4j.Logger;
import javax.swing.*;
import java.io.IOException;


public class Main {
    private static Logger  logger = Logger.getLogger(Main.class);
    private Main()
    {

        logger.info("Opening application.");

    }


    public static void main(String[] args) {
        System.out.println("Hello World!");

        //Tworzenie GUI poprzez main()
       SwingUtilities.invokeLater(() -> {
           try{

               GUI.createAndShowGUI();
               new Main();
           }
           catch (IOException ioe)
           {

               logger.error("In createAndShowGUI() - ",ioe);

           }



       });



    }
}
