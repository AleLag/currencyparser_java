import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;


// Klasa do tworzenia grafu, wykorzystuje pakiet JFreeChart
class Plot extends JFrame
{

    Plot(String title)
    {
        super(title);
        // Create dataset
        XYDataset dataset = createDataset();

        JFreeChart chart = ChartFactory.createTimeSeriesChart("Monthly currency rate for " + DownloadFile.getCode(),"Date","Value [PLN]", dataset);


        XYPlot plot = (XYPlot) chart.getPlot();
        //Format osi X
        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yyyy"));
        //Background
        plot.setBackgroundPaint(Color.WHITE);

        //Create panel

        ChartPanel panel = new ChartPanel(chart);
        setContentPane(panel);
    }
    //Tworzenie zbioru punktow do grafu
        private XYDataset createDataset()
        {
            TimeSeriesCollection dataset = new TimeSeriesCollection();
            TimeSeries unit = new TimeSeries(DownloadFile.getCode());
            for( int i = 1; i<DownloadFile.getHistValues().size() ; i++)
            {

                unit.addOrUpdate(new Day(DownloadFile.getHistDates().get(i)),DownloadFile.getHistValues().get(i));

            }
            dataset.addSeries(unit);
            return dataset;

        }



}

